FROM debian:buster-slim

RUN apt-get update
RUN apt-get install -y gnupg wget curl
RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add -
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list
RUN apt-get update

RUN apt-get install -y mongocli

COPY entrypoint.sh .
ENTRYPOINT [ "./entrypoint.sh" ]